// Created by Roman Voinitchi on 12/22/20
// 


import Foundation
import Vision

struct CheckSelectionBox {
    
    let double: Double
    let boundingBox: CGRect
    var imageBox: CGRect = .zero
}
